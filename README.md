# MatrixTexting

This is an Android App that will bridge SMS/MMS messages between your cellphone and a matrix server.

The app will watch for incoming messages and forward them to a matrix room based on the cellphone
 number. If there is no existing room the app will create a new room and invite the user to a
 conversation. You may also create a room, invite the bot, and set the topic to the cellphone number
 to start a new conversation.

Once we have a room for a number, all new messages will be bridged.

This app uses a database to keep track of the messages to/from Matrix. This is useful to keep track
 of messages that have failed to send so we can try to resend them at a later time.

**NOTE:** This app does not currently support end to end encryption. For this reason it is
 recommended to set up your own self hosted synapse server.


## Install  

0.  Setup your own homeserver.. or use matrix.org via riot - https://riot.im/app/#/register
1.  If you don't have an account, Create one.
2.  Create an account for the bot.
3.  Install the latest version https://gitlab.com/untidylamp/MatrixTexting/tags
4.  Select "Settings" from the menu in the top right.
5.  "This Cell Number" - Not used (yet) cant be null.
6.  "Home Server" - URL is either the one you set up or https://matrix.org
7.  "Matrix User" - is your account
8.  Fill in the bot information in the next two fields
9.  "Matrix Sync Delay" is the amount of seconds the app waits before checking for new Matrix Messages.
10.  "Matrix Sync Timeout" is the amount of seconds the app will wait for Matrix to respond.
11.  Push Login. You will get a notification letting you know if it was successful.
12.  Backout of this page and start the service.

It's recommended to disable Battery Optimisation so this app can run in the background.


## Status

If you have any requests feel free to open an issue.


**Android -> Matrix**
* [X]  SMS
* [X]  MMS (queries android smsmms.db for new messages)
* [X]  Room Creation on new conversation.
* [ ]  Group Messages
* [ ]  SMS with subject


**Matrix -> Android**
* [X]  m.text - sent as SMS
* [X]  m.image - Sent as URL. I'd like to send an MMS message (Help Wanted).
* [X]  m.video - Sent as URL.
* [X]  Initiate a new conversation
* [ ]  Group Messages. Needs to be sent as an MMS message (Help Wanted).

**Admin Room**
* [ ]  List all rooms bot is in
* [ ]  Leave a room
* [ ]  Reset locked messages
* [ ]  Reset failed messages
* [ ]  List pending messages
* [ ]  Send pending messages
* [ ]  Delete message database
* [ ]  Delete rooms database


## TODO:

* Clean up TODOs in App code.
* Features noted above.
* Confirm SMS/MMS messages were actually sent before marking as sent in Database.
* Send MMS messages as MMS and not a link
* Encryption
* Battery Optimisation kills app. Requires user to manually disable it.

## Contributing:

Please feel free to open issues or Merge Requests.