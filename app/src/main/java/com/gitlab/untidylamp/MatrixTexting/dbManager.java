package com.gitlab.untidylamp.MatrixTexting;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class dbManager {
    private static dbManager ourInsance = null;
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.dbManager";
    public static final int TYPE_INCOMING = 1;
    public static final int TYPE_OUTGOING = 2;
    public static final int TYPE_RESYNC = 3;
    public static final int BRIDGED_NO = 0;
    public static final int BRIDGED_YES = 1;
    public static final int BRIDGED_INIT = 2;
    public static final int LOCK_NO = 0;
    public static final int LOCK_YES = 1;
    public static final int ERROR_NO = 0;
    public static final int ERROR_NETWORK = 1;
    public static final int ERROR_MATRIX = 2;
    public static final int ERROR_UNEXPECTED = 3;
    public static final int ERROR_UPLOAD_CANCELED = 4;
    public static final int ERROR_UPLOAD_ERROR = 5;
    public static final int EPOCH_NO = 0;
    public static final String EMPTY = "";

    private static final String[] ALLColumns = new String[] { "*" };


    public static dbManager getInstance() {
        if(ourInsance == null) {
            ourInsance = new dbManager();
        }
        return ourInsance;
    }

    public static long dumpSmses(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "dumpSmses");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, ALLColumns, null, null, null, null, null);
        long smsCount=smsQueryCursor.getCount();
        Log.i(LOG_TAG, "Heres what's in the cursor " + DatabaseUtils.dumpCursorToString(smsQueryCursor)  );
        smsQueryCursor.close();
        db.close();

        Log.i(LOG_TAG, "dumpSmses is done");
        return smsCount;
    }

    public static long dumpMmses(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "dumpMmses");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, ALLColumns, null, null, null, null, null);
        Log.i(LOG_TAG, "Heres what's in the cursor " + DatabaseUtils.dumpCursorToString(mmsQueryCursor)  );
        long mmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();
        db.close();

        Log.i(LOG_TAG, "dumpMmses is done");
        return mmsCount;
    }

    public static long countMmses(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "countMmses");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, ALLColumns, null, null, null, null, null);
        long mmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();
        db.close();

        Log.i(LOG_TAG, "countMmses is done");
        return mmsCount;
    }

    public static long importNewSms(SqlOpenHelper dbHelper, int type, long sourceEpoch, long recievedEpoch, String number, String message, int bridged, int lock, int error) {
        Log.i(LOG_TAG, "importNewSms");
        long smsImportRowId = -1;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.SmsEntry.COLUMN_TYPE, type);
        values.put(DatabaseContract.SmsEntry.COLUMN_SOURCE_EPOCH, sourceEpoch);
        values.put(DatabaseContract.SmsEntry.COLUMN_RECIEVED_EPOCH, recievedEpoch);
        values.put(DatabaseContract.SmsEntry.COLUMN_NUMBER, number);
        values.put(DatabaseContract.SmsEntry.COLUMN_MESSAGE, message);
        values.put(DatabaseContract.SmsEntry.COLUMN_BRIDGED, bridged);
        values.put(DatabaseContract.SmsEntry.COLUMN_LOCK, lock);
        values.put(DatabaseContract.SmsEntry.COLUMN_ERROR, error);
        try {
            smsImportRowId = db.insert(DatabaseContract.SmsEntry.TABLE_NAME, null, values);
            Log.i(LOG_TAG, "importNewSms  " + smsImportRowId  );
        } catch (Exception e) {
            Log.e(LOG_TAG, "importNewSms failed  " + e  );
            smsImportRowId = -1;
        } finally {
            db.close();
            return smsImportRowId;
        }
    }

    public static void updateLongSmsBody (SqlOpenHelper dbHelper, long id, String body ){
        Log.i(LOG_TAG, "updateLongSmsBody");
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.SmsEntry.COLUMN_MESSAGE, body);
        try {
            Log.i(LOG_TAG, " try updateLongSmsBody _id=" + id  );
            db.update(DatabaseContract.SmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "updateLongSmsBody failed  " + e  );
        } finally {
            db.close();
        }
    }

    public static long importNewMms(SqlOpenHelper dbHelper, int type, long sourceEpoch, long recievedEpoch, String number, long mmsId, String url, int bridged, int lock, int error) {
        Log.i(LOG_TAG, "importNewMms");
        long mmsImportRowId = -1;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.MmsEntry.COLUMN_TYPE, type);
        values.put(DatabaseContract.MmsEntry.COLUMN_SOURCE_EPOCH, sourceEpoch);
        values.put(DatabaseContract.MmsEntry.COLUMN_RECIEVED_EPOCH, recievedEpoch);
        values.put(DatabaseContract.MmsEntry.COLUMN_NUMBER, number);
        values.put(DatabaseContract.MmsEntry.COLUMN_MMS_ID, mmsId);
        values.put(DatabaseContract.MmsEntry.COLUMN_URL, url);
        values.put(DatabaseContract.MmsEntry.COLUMN_BRIDGED, bridged);
        values.put(DatabaseContract.MmsEntry.COLUMN_LOCK, lock);
        values.put(DatabaseContract.MmsEntry.COLUMN_ERROR, error);
        try {
            mmsImportRowId = db.insert(DatabaseContract.MmsEntry.TABLE_NAME, null, values);
            Log.i(LOG_TAG, "importNewMms  " + mmsImportRowId  );
        } catch (Exception e) {
            Log.e(LOG_TAG, "importNewMms failed  " + e  );
            mmsImportRowId = -1;
        } finally {
            db.close();
            return mmsImportRowId;
        }
    }

    public static long lastMmsId(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "lastMmsId");
        int mmsId = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, ALLColumns, null, null, null, null, "mms_id DESC");

        try {
            mmsQueryCursor.moveToFirst();
            mmsId = mmsQueryCursor.getInt(mmsQueryCursor.getColumnIndex("mms_id"));
        } catch (Exception e) {
            Log.e(LOG_TAG, "lastMmsId ERROR: " + e);
            mmsId = 0;
        } finally {
            mmsQueryCursor.close();
            db.close();
            Log.i(LOG_TAG, "lastMmsId is done");
            return mmsId;
        }
    }

    public static long lastReceivedMmsId(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "lastMmsId");
        int mmsId = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " type=" +TYPE_INCOMING + " OR type=" + TYPE_RESYNC + " ";
        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, "mms_id DESC");

        try {
            mmsQueryCursor.moveToFirst();
            mmsId = mmsQueryCursor.getInt(mmsQueryCursor.getColumnIndex("mms_id"));
        } catch (Exception e) {
            Log.e(LOG_TAG, "lastMmsId ERROR: " + e);
            mmsId = 0;
        } finally {
            mmsQueryCursor.close();
            db.close();
            Log.i(LOG_TAG, "lastMmsId is done");
            return mmsId;
        }
    }

    public static void smsLock (SqlOpenHelper dbHelper, long id ){
        Log.i(LOG_TAG, "smsLock: " + id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.SmsEntry.COLUMN_LOCK, LOCK_YES);
        try {
            Log.i(LOG_TAG, " try smsLock _id=" + id  );
            db.update(DatabaseContract.SmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "smsLock: " + id + " failed  " + e  );
        } finally {
            db.close();
        }
        Log.i(LOG_TAG, "smsLock: " + id + " is done");
    }

    public static void mmsLock (SqlOpenHelper dbHelper, long id ){
        Log.i(LOG_TAG, "mmsLock: " + id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.MmsEntry.COLUMN_LOCK, LOCK_YES);
        try {
            Log.i(LOG_TAG, " try mmsLock _id=" + id  );
            db.update(DatabaseContract.MmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "mmsLock: " + id + " failed  " + e  );
        } finally {
            db.close();
        }
        Log.i(LOG_TAG, "mmsLock: " + id + " is done");
    }

    public static void smsUnlock (SqlOpenHelper dbHelper, long id ){
        Log.i(LOG_TAG, "smsUnlock: " + id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.SmsEntry.COLUMN_LOCK, LOCK_NO);
        try {
            Log.i(LOG_TAG, " try smsUnlock _id=" + id  );
            db.update(DatabaseContract.SmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "smsUnlock: " + id + " failed  " + e  );
        } finally {
            db.close();
        }
        Log.i(LOG_TAG, "smsUnlock: " + id + " is done");
    }

    public static void mmsUnlock (SqlOpenHelper dbHelper, long id ){
        Log.i(LOG_TAG, "mmsUnlock: " + id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.MmsEntry.COLUMN_LOCK, LOCK_NO);
        try {
            Log.i(LOG_TAG, " try mmsUnlock _id=" + id  );
            db.update(DatabaseContract.MmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "mmsUnlock: " + id + " failed  " + e  );
        } finally {
            db.close();
        }
        Log.i(LOG_TAG, "mmsUnlock: " + id + " is done");
    }

    public static void smsBridged (SqlOpenHelper dbHelper, long id ){
        Log.i(LOG_TAG, "smsBridged: " + id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.SmsEntry.COLUMN_LOCK, LOCK_NO);
        values.put(DatabaseContract.SmsEntry.COLUMN_BRIDGED, BRIDGED_YES);
        try {
            Log.i(LOG_TAG, " try smsBridged _id=" + id  );
            db.update(DatabaseContract.SmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "smsBridged: " + id + " failed  " + e  );
        } finally {
            db.close();
        }
        Log.i(LOG_TAG, "smsBridged: " + id + " is done");
    }

    public static void mmsBridged (SqlOpenHelper dbHelper, long id ){
        Log.i(LOG_TAG, "mmsBridged: " + id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.MmsEntry.COLUMN_LOCK, LOCK_NO);
        values.put(DatabaseContract.MmsEntry.COLUMN_BRIDGED, BRIDGED_YES);
        try {
            Log.i(LOG_TAG, " try mmsBridged _id=" + id  );
            db.update(DatabaseContract.MmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "mmsBridged: " + id + " failed  " + e  );
        } finally {
            db.close();
        }
        Log.i(LOG_TAG, "mmsBridged: " + id + " is done");
    }

    public static void smsError (SqlOpenHelper dbHelper, long id , int error){
        Log.i(LOG_TAG, "smsError: " + id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.SmsEntry.COLUMN_BRIDGED, BRIDGED_NO);
        values.put(DatabaseContract.SmsEntry.COLUMN_LOCK, LOCK_NO);
        values.put(DatabaseContract.SmsEntry.COLUMN_ERROR, error);
        try {
            Log.i(LOG_TAG, " try smsError _id=" + id  );
            db.update(DatabaseContract.SmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "smsError: " + id + " failed  " + e  );
        } finally {
            db.close();
        }
        Log.i(LOG_TAG, "smsError: " + id + " is done");
    }

    public static void mmsError (SqlOpenHelper dbHelper, long id , int error){
        Log.i(LOG_TAG, "mmsError: " + id);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.MmsEntry.COLUMN_BRIDGED, BRIDGED_NO);
        values.put(DatabaseContract.MmsEntry.COLUMN_LOCK, LOCK_NO);
        values.put(DatabaseContract.MmsEntry.COLUMN_ERROR, error);
        try {
            Log.i(LOG_TAG, " try mmsError _id=" + id  );
            db.update(DatabaseContract.MmsEntry.TABLE_NAME, values, "_id=" + id, null );
        } catch (Exception e) {
            Log.e(LOG_TAG, "mmsError: " + id + " failed  " + e  );
        } finally {
            db.close();
        }
        Log.i(LOG_TAG, "mmsError: " + id + " is done");
    }

    public static long dumpRooms(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "dumpRooms");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor roomsQueryCursor = db.query(DatabaseContract.RoomsEntry.TABLE_NAME, ALLColumns, null, null, null, null, null);
        long roomsCount=roomsQueryCursor.getCount();
        Log.i(LOG_TAG, "Heres what's in the roomsQueryCursor " + DatabaseUtils.dumpCursorToString(roomsQueryCursor)  );
        roomsQueryCursor.close();
        db.close();

        Log.i(LOG_TAG, "dumpRooms is done");
        return roomsCount;
    }

    public static long newRoom(SqlOpenHelper dbHelper, String number) {
        long newRoomRowId = -1;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.RoomsEntry.COLUMN_NUMBER, number);
        values.put(DatabaseContract.RoomsEntry.COLUMN_MATRIX_ROOM, EMPTY);
        values.put(DatabaseContract.RoomsEntry.COLUMN_LOCK, LOCK_YES);
        try {
            newRoomRowId = db.insert(DatabaseContract.RoomsEntry.TABLE_NAME, null, values);
            Log.i(LOG_TAG, "newRoom  " + newRoomRowId  );
        } catch (Exception e) {
            Log.e(LOG_TAG, "newRoom failed  " + e  );
            newRoomRowId = -1;
        } finally {
            db.close();
            return newRoomRowId;
        }
    }

    public static Boolean updateNewRoom (SqlOpenHelper dbHelper, long id, String matrix_room ){
        Log.i(LOG_TAG, "updateNewRoom");
        Boolean updateRoom = false;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.RoomsEntry.COLUMN_MATRIX_ROOM, matrix_room);
        values.put(DatabaseContract.RoomsEntry.COLUMN_LOCK, LOCK_NO);
        try {
            Log.i(LOG_TAG, " try updateNewRoom _id=" + id  );
            db.update(DatabaseContract.RoomsEntry.TABLE_NAME, values, "_id=" + id, null );
            updateRoom = true;
        } catch (Exception e) {
            Log.e(LOG_TAG, "updateNewRoom failed  " + e  );
        } finally {
            db.close();
            return updateRoom;
        }
    }

    public static String getMatrixRoom(SqlOpenHelper dbHelper, String number){
        Log.i(LOG_TAG, "getMatrixRoom");
        String matrixRoom="ERROR";
        int lock=-1;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " number='" + number + "' ";
        Cursor getRoomQueryCursor = db.query(DatabaseContract.RoomsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);

        try {
            getRoomQueryCursor.moveToFirst();
            matrixRoom = getRoomQueryCursor.getString(getRoomQueryCursor.getColumnIndex("matrix_room"));
            lock = getRoomQueryCursor.getInt(getRoomQueryCursor.getColumnIndex("lock"));
        } catch (Exception e) {
            Log.e(LOG_TAG, "getMatrixRoom ERROR: " + e);
            matrixRoom = "ERROR";
        } finally {
            getRoomQueryCursor.close();
            db.close();
        }
        if (lock == LOCK_YES) {
            matrixRoom = "LOCKED";
        }

        return matrixRoom;
    }

    public static String getMatrixRoomNumber(SqlOpenHelper dbHelper, String matrixRoomId){
        Log.i(LOG_TAG, "getMatrixRoomNumber");
        String matrixRoomNumber = "";
        int lock=-1;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = "lock=0 AND matrix_room='" + matrixRoomId + "' ";
        Cursor getRoomQueryCursor = db.query(DatabaseContract.RoomsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);

        try {
            getRoomQueryCursor.moveToFirst();
            matrixRoomNumber = getRoomQueryCursor.getString(getRoomQueryCursor.getColumnIndex("number"));
        } catch (Exception e) {
            Log.e(LOG_TAG, "getMatrixRoom ERROR: " + e);
        } finally {
            getRoomQueryCursor.close();
            db.close();
        }
        return matrixRoomNumber;
    }

    public static Boolean deleteRoom (SqlOpenHelper dbHelper, String matrixRoomId){
        Log.i(LOG_TAG, "deleteRoom");
        Boolean deleteRoom = false;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            db.delete(DatabaseContract.RoomsEntry.TABLE_NAME, "matrix_room='"+matrixRoomId+"'",null);
            deleteRoom = true;
        } catch (Exception e) {
            Log.e(LOG_TAG, "deleteRoom failed  " + e  );

        } finally {
            db.close();
            return deleteRoom;
        }
    }

    public static long addMatrixRoom (SqlOpenHelper dbHelper, String matrixRoomId){
        long newRoomRowId = -1;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        int numberOfRooms = 0;


        String selectionPart = "matrix_room='"+matrixRoomId+"'";
        Cursor roomssQueryCursor = db.query(DatabaseContract.RoomsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        try {
            roomssQueryCursor.moveToFirst();
            numberOfRooms = roomssQueryCursor.getCount();
        } catch (Exception e) {
            Log.e(LOG_TAG, "lastMmsId ERROR: " + e);
            numberOfRooms = 0;
        } finally {
            roomssQueryCursor.close();
        }

        if (numberOfRooms == 0) {
            ContentValues values = new ContentValues();
            values.put(DatabaseContract.RoomsEntry.COLUMN_NUMBER, EMPTY);
            values.put(DatabaseContract.RoomsEntry.COLUMN_MATRIX_ROOM, matrixRoomId);
            values.put(DatabaseContract.RoomsEntry.COLUMN_LOCK, LOCK_YES);
            try {
                newRoomRowId = db.insert(DatabaseContract.RoomsEntry.TABLE_NAME, null, values);
                Log.i(LOG_TAG, "addMatrixRoom  " + newRoomRowId  );
            } catch (Exception e) {
                Log.e(LOG_TAG, "addMatrixRoom failed  " + e  );
                newRoomRowId = -1;
            }
        }

        db.close();

        return newRoomRowId;

    }

    public static Boolean updateRoomTopic (SqlOpenHelper dbHelper, String matrixRoomId, String phoneNumber ){
        Log.i(LOG_TAG, "updateRoomTopic");
        Boolean updateRoomTopic = false;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.RoomsEntry.COLUMN_NUMBER, phoneNumber);
        values.put(DatabaseContract.RoomsEntry.COLUMN_LOCK, LOCK_NO);
        try {
            Log.i(LOG_TAG, " try updateRoomTopic lock=1 AND matrix_room='" +matrixRoomId+"'"  );

            int changeCount = db.update(DatabaseContract.RoomsEntry.TABLE_NAME, values, "lock=1 AND matrix_room='" + matrixRoomId +"' " , null );
            if (changeCount > 0) {
                updateRoomTopic = true;
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, "updateRoomTopic failed  " + e  );
        } finally {
            db.close();
            return updateRoomTopic;
        }
    }

    public static long inSmsCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "inSmsCount");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " type=" + TYPE_INCOMING ;
        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long inSmsCount = smsQueryCursor.getCount();
        smsQueryCursor.close();
        db.close();
        Log.i(LOG_TAG, "inSmsCount is done");
        return inSmsCount;
    }

    public static long inMmsCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "inMmsCount");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " type=" + TYPE_INCOMING ;
        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long inMmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();
        db.close();
        Log.i(LOG_TAG, "inMmsCount is done");
        return inMmsCount;
    }

    public static long outSmsCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "outSmsCount");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " type=" + TYPE_OUTGOING ;
        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long outSmsCount = smsQueryCursor.getCount();
        smsQueryCursor.close();
        db.close();
        Log.i(LOG_TAG, "outSmsCount is done");
        return outSmsCount;
    }

    public static long outMmsCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "outMmsCount");

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " type=" + TYPE_OUTGOING ;
        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long outMmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();
        db.close();
        Log.i(LOG_TAG, "outMmsCount is done");
        return outMmsCount;
    }

    public static long totalMessageCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "totalMessageCount");
        long totalMessageCount = 0;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " ( type=" + TYPE_INCOMING + " OR type=" +TYPE_OUTGOING + " ) " ;

        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long smsCount = smsQueryCursor.getCount();
        smsQueryCursor.close();

        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long mmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();

        db.close();

        totalMessageCount = smsCount + mmsCount;
        Log.i(LOG_TAG, "inSmsCount is done");
        return totalMessageCount;
    }

    public static long totalBridgedCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "totalMessageCount");
        long totalMessageCount = 0;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " ( type=" + TYPE_INCOMING + " OR type=" + TYPE_OUTGOING + " ) AND bridged=" + BRIDGED_YES ;

        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long smsCount = smsQueryCursor.getCount();
        smsQueryCursor.close();

        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long mmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();

        db.close();

        totalMessageCount = smsCount + mmsCount;
        Log.i(LOG_TAG, "inSmsCount is done");
        return totalMessageCount;
    }

    public static long totalPendingCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "totalMessageCount");
        long totalMessageCount = 0;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " ( type=" + TYPE_INCOMING + " OR type=" + TYPE_OUTGOING + " ) AND bridged=" + BRIDGED_NO ;

        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long smsCount = smsQueryCursor.getCount();
        smsQueryCursor.close();

        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long mmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();

        db.close();

        totalMessageCount = smsCount + mmsCount;
        Log.i(LOG_TAG, "inSmsCount is done");
        return totalMessageCount;
    }

    public static long totalLockedCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "totalMessageCount");
        long totalMessageCount = 0;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " ( type=" + TYPE_INCOMING + " OR type=" + TYPE_OUTGOING + " ) AND bridged=" + BRIDGED_NO + " AND lock=" +LOCK_YES ;

        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long smsCount = smsQueryCursor.getCount();
        smsQueryCursor.close();

        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long mmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();

        db.close();

        totalMessageCount = smsCount + mmsCount;
        Log.i(LOG_TAG, "inSmsCount is done");
        return totalMessageCount;
    }

    public static long totalErrorCount(SqlOpenHelper dbHelper) {
        Log.i(LOG_TAG, "totalMessageCount");
        long totalMessageCount = 0;

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String selectionPart = " ( type=" + TYPE_INCOMING + " OR type=" + TYPE_OUTGOING + " ) AND bridged=" + BRIDGED_NO + " AND NOT error=" +ERROR_NO ;

        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long smsCount = smsQueryCursor.getCount();
        smsQueryCursor.close();

        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);
        long mmsCount = mmsQueryCursor.getCount();
        mmsQueryCursor.close();

        db.close();

        totalMessageCount = smsCount + mmsCount;
        Log.i(LOG_TAG, "inSmsCount is done");
        return totalMessageCount;
    }

    public static Boolean deleteAllMessages (SqlOpenHelper dbHelper){
        Log.i(LOG_TAG, "deleteAllMessages");
        Boolean deleteAllMessages = false;


        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            db.delete(DatabaseContract.SmsEntry.TABLE_NAME, null,null);
            db.delete(DatabaseContract.MmsEntry.TABLE_NAME, null,null);

            deleteAllMessages = true;
        } catch (Exception e) {
            Log.e(LOG_TAG, "deleteAllMessages failed  " + e  );

        } finally {
            db.close();
            return deleteAllMessages;
        }
    }

    public static Boolean deleteAllMmses (SqlOpenHelper dbHelper){
        Log.i(LOG_TAG, "deleteAllMmses");
        Boolean deleteAllMmses = false;


        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            db.delete(DatabaseContract.MmsEntry.TABLE_NAME, null,null);

            deleteAllMmses = true;
        } catch (Exception e) {
            Log.e(LOG_TAG, "deleteAllMmses failed  " + e  );

        } finally {
            db.close();
            return deleteAllMmses;
        }
    }

    public static Boolean deleteAllRooms (SqlOpenHelper dbHelper){
        Log.i(LOG_TAG, "deleteAllRooms");
        Boolean deleteAllRooms = false;


        SQLiteDatabase db = dbHelper.getWritableDatabase();
        try {
            db.delete(DatabaseContract.RoomsEntry.TABLE_NAME, null,null);

            deleteAllRooms = true;
        } catch (Exception e) {
            Log.e(LOG_TAG, "deleteAllRooms failed  " + e  );

        } finally {
            db.close();
            return deleteAllRooms;
        }
    }

}
