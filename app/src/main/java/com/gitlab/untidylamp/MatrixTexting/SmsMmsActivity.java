package com.gitlab.untidylamp.MatrixTexting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class SmsMmsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsmms);

        final TextView inSms = findViewById(R.id.smsmms_in_sms);
        final TextView inMms = findViewById(R.id.smsmms_in_mms);
        final TextView outSms = findViewById(R.id.smsmms_out_sms);
        final TextView outMms = findViewById(R.id.smsmms_out_mms);
        final TextView total = findViewById(R.id.smsmms_total);
        final TextView bridged = findViewById(R.id.smsmms_bridged);
        final TextView pending = findViewById(R.id.smsmms_pending);
        final TextView locked = findViewById(R.id.smsmms_locked);
        final TextView error = findViewById(R.id.smsmms_error);




        final Object updateButton = findViewById(R.id.smsmms_update_button);
        ((View) updateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(getApplicationContext());
                inSms.setText(Long.toString(dbManager.inSmsCount(mSqlOpenHelper)));
                inMms.setText(Long.toString(dbManager.inMmsCount(mSqlOpenHelper)));
                outSms.setText(Long.toString(dbManager.outSmsCount(mSqlOpenHelper)));
                outMms.setText(Long.toString(dbManager.outMmsCount(mSqlOpenHelper)));
                total.setText(Long.toString(dbManager.totalMessageCount(mSqlOpenHelper)));
                bridged.setText(Long.toString(dbManager.totalBridgedCount(mSqlOpenHelper)));
                pending.setText(Long.toString(dbManager.totalPendingCount(mSqlOpenHelper)));
                locked.setText(Long.toString(dbManager.totalLockedCount(mSqlOpenHelper)));
                error.setText(Long.toString(dbManager.totalErrorCount(mSqlOpenHelper)));

                mSqlOpenHelper.close();
            }
        });


        final Object resetButton = findViewById(R.id.smsmms_delete_button);
        ((View) resetButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mmsIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MmsService.class);
                stopService(mmsIntent);
                Intent MatrixIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MatrixService.class);
                stopService(MatrixIntent);
                SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(getApplicationContext());
                dbManager.deleteAllMessages(mSqlOpenHelper);
                mSqlOpenHelper.close();
            }
        });



    }


}
