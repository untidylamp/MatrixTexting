package com.gitlab.untidylamp.MatrixTexting;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonObject;

import org.matrix.androidsdk.data.MyUser;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.RoomState;
import org.matrix.androidsdk.listeners.IMXEventListener;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.User;
import org.matrix.androidsdk.rest.model.bingrules.BingRule;

import java.util.List;

public class MatrixEventListener implements IMXEventListener {
    private Matrix matrix = null;
    private String matrixUser;
    private Context context;
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.MatrixEventListener";
    private Boolean onInitialSyncComplete = false;

    public MatrixEventListener (Matrix matrix, Context context, String matrixUser) {
        Log.i(LOG_TAG, "MatrixEventListener    event listener");
        this.context = context;
        this.matrix = matrix;
        this.matrixUser = matrixUser;
    }

    @Override
    public void onLiveEvent(Event event, RoomState roomState) {
        Log.i(LOG_TAG, "onLiveEvent    event listener");
        if (onInitialSyncComplete) {
            if (event.sender != null && event.sender.equals(matrixUser)) {
                Log.i(LOG_TAG, "message from matrix user. ");
                Room room = matrix.store.getRoom(event.roomId);


                if (event.type.equals("m.room.message")) {
                    matrix.matrixRoomMessage(room, event);

                } else if (event.type.equals("m.room.member")) {
                    matrix.matrixRoomMember(room, event);

                } else if (event.type.equals("m.room.topic")) {
                    matrix.matrixRoomTopic(room, event);

                }

                SmsMms smsMms = new SmsMms(context);
                smsMms.sendUnreadSmses();
                smsMms.sendUnreadMmsesAsUrl();
            }
        }
    }


    @Override
    public void onInitialSyncComplete(String s) {
        Log.i(LOG_TAG, "onInitialSyncComplete    event listener");
        onInitialSyncComplete = true;
    }



    @Override
    public void onIgnoredUsersListUpdate() {
        Log.i(LOG_TAG, "onIgnoredUsersListUpdate    event listener");

    }

    @Override
    public void onDirectMessageChatRoomsListUpdate() {
        Log.i(LOG_TAG, "onDirectMessageChatRoomsListUpdate    event listener");
    }

    @Override
    public void onLiveEventsChunkProcessed(String fromToken, String toToken) {
        Log.i(LOG_TAG, "onLiveEventsChunkProcessed    event listener");

    }

    @Override
    public void onBingEvent(Event event, RoomState roomState, BingRule bingRule) {
        Log.i(LOG_TAG, "onBingEvent    event listener");

    }

    @Override
    public void onEventSentStateUpdated(Event event) {
        Log.i(LOG_TAG, "onEventSentStateUpdated    event listener");

    }

    @Override
    public void onEventSent(Event event, String prevEventId) {
        Log.i(LOG_TAG, "onEventSent    event listener");

    }

    @Override
    public void onEventDecrypted(Event event) {
        Log.i(LOG_TAG, "onEventDecrypted    event listener");

    }

    @Override
    public void onBingRulesUpdate() {
        Log.i(LOG_TAG, "onBingRulesUpdate    event listener");

    }

    @Override
    public void onSyncError(MatrixError matrixError) {
        Log.i(LOG_TAG, "onSyncError    event listener");

    }

    @Override
    public void onCryptoSyncComplete() {
        Log.i(LOG_TAG, "onCryptoSyncComplete    event listener");

    }

    @Override
    public void onNewRoom(String roomId) {
        Log.i(LOG_TAG, "onNewRoom    event listener");

    }

    @Override
    public void onJoinRoom(String roomId) {
        Log.i(LOG_TAG, "onJoinRoom    event listener");
        matrix.sendUnreadSmses();
        matrix.sendUnreadMmses();
    }

    @Override
    public void onRoomFlush(String roomId) {
        Log.i(LOG_TAG, "onRoomFlush    event listener");

    }

    @Override
    public void onRoomInternalUpdate(String roomId) {
        Log.i(LOG_TAG, "onRoomInternalUpdate    event listener");

    }

    @Override
    public void onNotificationCountUpdate(String roomId) {
        Log.i(LOG_TAG, "onNotificationCountUpdate    event listener");

    }

    @Override
    public void onLeaveRoom(String roomId) {
        Log.i(LOG_TAG, "onLeaveRoom    event listener");

        }

    @Override
    public void onRoomKick(String roomId) {
        Log.i(LOG_TAG, "onRoomKick    event listener");

    }

    @Override
    public void onReceiptEvent(String roomId, List<String> senderIds) {
        Log.i(LOG_TAG, "onReceiptEvent    event listener");

    }

    @Override
    public void onRoomTagEvent(String roomId) {
        Log.i(LOG_TAG, "onRoomTagEvent    event listener");

    }

    @Override
    public void onReadMarkerEvent(String roomId) {
        Log.i(LOG_TAG, "onReadMarkerEvent    event listener");

    }

    @Override
    public void onToDeviceEvent(Event event) {
        Log.i(LOG_TAG, "onToDeviceEvent    event listener");

    }

    @Override
    public void onNewGroupInvitation(String groupId) {
        Log.i(LOG_TAG, "onNewGroupInvitation    event listener");

    }

    @Override
    public void onJoinGroup(String groupId) {
        Log.i(LOG_TAG, "onJoinGroup    event listener");

    }

    @Override
    public void onLeaveGroup(String groupId) {
        Log.i(LOG_TAG, "onLeaveGroup    event listener");

    }

    @Override
    public void onGroupProfileUpdate(String groupId) {
        Log.i(LOG_TAG, "onGroupProfileUpdate    event listener");

    }

    @Override
    public void onGroupRoomsListUpdate(String groupId) {
        Log.i(LOG_TAG, "onGroupRoomsListUpdate    event listener");

    }

    @Override
    public void onGroupUsersListUpdate(String groupId) {
        Log.i(LOG_TAG, "onGroupUsersListUpdate    event listener");

    }

    @Override
    public void onGroupInvitedUsersListUpdate(String groupId) {
        Log.i(LOG_TAG, "onGroupInvitedUsersListUpdate    event listener");

    }

    @Override
    public void onAccountDataUpdated() {
        Log.i(LOG_TAG, "onAccountDataUpdated    event listener");

    }

    @Override
    public void onAccountInfoUpdate(MyUser myUser) {
        Log.i(LOG_TAG, "onAccountInfoUpdate    event listener");

    }

    @Override
    public void onPresenceUpdate(Event event, User user) {
        Log.i(LOG_TAG, "onPresenceUpdate    event listener");

    }

    @Override
    public void onStoreReady() {
        Log.i(LOG_TAG, "onStoreReady    event listener");

    }

}
