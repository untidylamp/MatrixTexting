package com.gitlab.untidylamp.MatrixTexting;

import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.IBinder;
import android.util.Log;

public class MmsService extends Service {
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.MmsService";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG_TAG, "onStartCommand: MmsService");


        //todo: test if no MMSes are on phone - likely break soemthing

        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(getApplicationContext());

        Boolean firstStart = intent.getBooleanExtra("startButton", false);
        if (firstStart) {
            importMmsStartingPoint(mSqlOpenHelper, firstStart);
        }


        long appMmsId = getAppMmsId(mSqlOpenHelper);
        long androidMmsId = getAndroidMmsId(mSqlOpenHelper);

        if (androidMmsId == appMmsId) {
            Log.i(LOG_TAG, "No New MMS Messages");
        } else if (androidMmsId > appMmsId) {
            newMmsIdsToDatabase(appMmsId);
        } else if (androidMmsId < appMmsId) {
            //because user deleted MMSes from default app and our app needs to resync.
            dbManager.deleteAllMmses(mSqlOpenHelper);
            getAppMmsId(mSqlOpenHelper);
        }
        mSqlOpenHelper.close();


        Intent MatrixIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MatrixService.class);
        startService(MatrixIntent);

        return START_NOT_STICKY;
    }

    private long getAppMmsId(SqlOpenHelper mSqlOpenHelper){
        long lastBridgedMms = dbManager.lastReceivedMmsId(mSqlOpenHelper);
        if (lastBridgedMms == 0) {
            Log.i(LOG_TAG, "Importing MMS id as starting point");
            lastBridgedMms = importMmsStartingPoint(mSqlOpenHelper, false);
        }
        return lastBridgedMms;
    }

    private void newMmsIdsToDatabase(long lastBridgedMms) {
        Uri mmsInboxDatabase = Uri.parse("content://mms/inbox");
        String selectionPart = "_id > " + lastBridgedMms;
        Cursor mmsInboxDatabaseCursor = getContentResolver().query(
                mmsInboxDatabase,
                null,
                selectionPart,
                null,
                "_id ASC");
        while (mmsInboxDatabaseCursor.moveToNext()) {
            long mid = mmsInboxDatabaseCursor.getInt(mmsInboxDatabaseCursor.getColumnIndex("_id"));
            Log.i(LOG_TAG, "mid to look in part for: " + mid );
            mmsIdToDatabase(mid);
        }
    }

    private void mmsIdToDatabase(long mid) {
        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(this);
        //Get Phone number
        String sourceNumber = getMmsSourceNumber(mid);
        Log.i(LOG_TAG, "Picture from: " + sourceNumber);
        dbManager.importNewMms(mSqlOpenHelper,dbManager.TYPE_INCOMING,dbManager.EPOCH_NO,dbManager.EPOCH_NO, sourceNumber, mid,  dbManager.EMPTY, dbManager.BRIDGED_NO,dbManager.LOCK_NO,dbManager.ERROR_NO);
    }

    private String getMmsSourceNumber(long mid) {
        String sourceNumber = null;

        Uri mmsAddrDatabase = Uri.parse("content://mms/" + mid + "/addr/");
        String selectionPart = "type=137";
        //137 = source
        //151 = dest
        // if there is more than one 151 for the same MMS id, its a group message.
        String[] selectAddress = new String[] { "address" };
        Cursor mmsAddrDatabaseCursor = getContentResolver().query(
                mmsAddrDatabase,
                selectAddress,
                selectionPart,
                null,
                null);
        try {
            mmsAddrDatabaseCursor.moveToFirst();
            sourceNumber = mmsAddrDatabaseCursor.getString(mmsAddrDatabaseCursor.getColumnIndex("address"));
            //remove the +1 because I dont need them
            //new provider uses +1 :)
            //sourceNumber = sourceNumber.replace("+1", "");
        } catch (Exception e) {
            Log.e(LOG_TAG, "Error finding phone number of MID");
            sourceNumber = "0";
        } finally {
            mmsAddrDatabaseCursor.close();
        }

        return sourceNumber;
    }

    private long getAndroidMmsId(SqlOpenHelper mSqlOpenHelper) {
        int mmsId = 0;
        Uri mmsInboxDatabase = Uri.parse("content://mms/inbox");
        Cursor mmsInboxDatabaseCursor = getContentResolver().query(
                mmsInboxDatabase,
                null,
                null,
                null,
                "_id DESC");

        try {
            mmsInboxDatabaseCursor.moveToFirst();
            mmsId = mmsInboxDatabaseCursor.getInt(mmsInboxDatabaseCursor.getColumnIndex("_id"));
        } catch (Exception e) {
            Log.e(LOG_TAG, "ERROR gettings Latest MMS: " + e );
            mmsId = 0;
        } finally {
            mmsInboxDatabaseCursor.close();
        }

        return mmsId;
    }

    private long importMmsStartingPoint(SqlOpenHelper mSqlOpenHelper, Boolean startButton) {
        long mmsId = getAndroidMmsId(mSqlOpenHelper);
        Log.i(LOG_TAG, "Latest MMS message: " + mmsId);
        if (!startButton) {
            mmsId = mmsId-1;
            Log.i(LOG_TAG, "adding " + mmsId + " to database so mms from onRecieve is bridged.");
        }

        long now = System.currentTimeMillis();
        dbManager.importNewMms(mSqlOpenHelper, dbManager.TYPE_RESYNC, now, now, "0000000000", mmsId, dbManager.EMPTY, dbManager.BRIDGED_INIT, dbManager.LOCK_NO, dbManager.ERROR_NO );
        return mmsId;
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "onDestroy: MmsService");
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
