package com.gitlab.untidylamp.MatrixTexting;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

public class SettingsActivity extends AppCompatActivity {
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.SettingsActivity";
    private SharedPreferences settings;
    private EditText userCellNumber;
    private EditText homeServerUrl;
    private EditText matrixUser;
    private EditText botUser;
    private EditText botPassword;
    private EditText matrixSyncDelay;
    private EditText matrixSyncTimeout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        String mPhoneNumber = "";


        try {
            TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
            mPhoneNumber = tMgr.getLine1Number();
        } catch (Exception e){ }





        settings = getSharedPreferences("settings", Context.MODE_PRIVATE);

        userCellNumber = (EditText) findViewById(R.id.user_cell_number);
        userCellNumber.setText(settings.getString("userCellNumber", mPhoneNumber));

        homeServerUrl = (EditText) findViewById(R.id.home_server_url);
        homeServerUrl.setText(settings.getString("homeServerUrl", ""));

        matrixUser = (EditText) findViewById(R.id.matrix_user);
        matrixUser.setText(settings.getString("matrixUser", ""));

        botUser = (EditText) findViewById(R.id.bot_user);
        botUser.setText(settings.getString("botUser", ""));

        botPassword = (EditText) findViewById(R.id.bot_password);
        botPassword.setText(settings.getString("botPassword", ""));

        matrixSyncDelay = (EditText) findViewById(R.id.matrix_sync_delay);
        matrixSyncDelay.setText(settings.getString("matrixSyncDelay", "60"));

        matrixSyncTimeout = (EditText) findViewById(R.id.matrix_sync_timeout);
        matrixSyncTimeout.setText(settings.getString("matrixSyncTimeout", "6"));




        Object loginButton = findViewById(R.id.matrix_login_button);

        ((View) loginButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //todo: Delete rooms entries - if the user makes a mistake for matrixUser and a sms
                // is sent. Then the user fixes matrixUsers, all messages will continue going to
                // the old room.
                //todo: Do this on lloogggleeeout

                if ( !userCellNumber.getText().toString().isEmpty() &&
                        !homeServerUrl.getText().toString().isEmpty() &&
                        !matrixUser.getText().toString().isEmpty() &&
                        !botUser.getText().toString().isEmpty() &&
                        !botPassword.getText().toString().isEmpty() &&
                        !matrixSyncDelay.getText().toString().isEmpty() &&
                        !matrixSyncTimeout.getText().toString().isEmpty()
                ) {
                    saveLoginCredentials(getApplicationContext(),
                            getContentResolver(),
                            view,
                            userCellNumber.getText().toString(),
                            homeServerUrl.getText().toString(),
                            matrixUser.getText().toString(),
                            botUser.getText().toString(),
                            botPassword.getText().toString(),
                            matrixSyncDelay.getText().toString(),
                            matrixSyncTimeout.getText().toString()
                    );
                } else {
                    topSnackBar(view,"Missing information");
                }

            }
        });


        Object logoutButton = findViewById(R.id.matrix_logout_button);
        ((View) logoutButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mmsIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MmsService.class);
                stopService(mmsIntent);
                Intent MatrixIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MatrixService.class);
                stopService(MatrixIntent);
                removeLoginCredentials(getApplicationContext(), getContentResolver() );
                topSnackBar(view,"Logged out");

            }
        });

        Object deleteButton = findViewById(R.id.matrix_delete_button);
        ((View) deleteButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mmsIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MmsService.class);
                stopService(mmsIntent);
                Intent MatrixIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MatrixService.class);
                stopService(MatrixIntent);
                removeLoginCredentials(getApplicationContext(), getContentResolver());
                deleteDatabase();
                topSnackBar(view,"Logged out and local Database Deleted");
            }
        });
    }



    private void topSnackBar(View view, String message) {
        Snackbar snack = Snackbar.make(findViewById(android.R.id.content), message, Snackbar.LENGTH_LONG);
        view = snack.getView();
        FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
        params.gravity = Gravity.TOP;
        view.setLayoutParams(params);
        snack.show();
    }

    private void saveLoginCredentials (final Context context,
                                       ContentResolver contentResolver,
                                       View view,
                                       String userCellNumber,
                                       String homeServerUrl,
                                       String matrixUser,
                                       String botUser,
                                       String botPassword,
                                       String matrixSyncDelay,
                                       String matrixSyncTimeout) {

        Matrix matrix = new Matrix(context,
                contentResolver,
                homeServerUrl,
                matrixUser,
                botUser,
                botPassword,
                matrixSyncDelay,
                matrixSyncTimeout);

        if (matrix.hasCredentials()) {
            topSnackBar(view,"already logged in");
        } else {
            SharedPreferences.Editor editSettings = settings.edit();
            editSettings.putString("userCellNumber", userCellNumber);
            editSettings.putString("homeServerUrl", homeServerUrl);
            editSettings.putString("matrixUser", matrixUser);
            editSettings.putString("botUser", botUser);
            editSettings.putString("botPassword","");
            editSettings.putString("matrixSyncDelay", matrixSyncDelay);
            editSettings.putString("matrixSyncTimeout", matrixSyncTimeout);
            editSettings.apply();

            Intent mmsIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MmsService.class);
            stopService(mmsIntent);
            Intent MatrixIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MatrixService.class);
            stopService(MatrixIntent);

            matrix.login();
        }
    }

    private void removeLoginCredentials (final Context context,
                                         ContentResolver contentResolver) {
        Matrix matrix = new Matrix(context,
                contentResolver,
                null,
                null,
                null,
                null,
                null,
                null );
        matrix.logout();


    }

    private void deleteDatabase(){

        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(getApplicationContext());
        dbManager.deleteAllMessages(mSqlOpenHelper);
        dbManager.deleteAllRooms(mSqlOpenHelper);
        mSqlOpenHelper.close();
    }

}
