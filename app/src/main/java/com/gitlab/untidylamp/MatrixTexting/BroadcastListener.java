package com.gitlab.untidylamp.MatrixTexting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class BroadcastListener extends BroadcastReceiver {
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.BroadcastListener";
    private static final String ACTION_MMS_RECEIVED = "android.provider.Telephony.WAP_PUSH_RECEIVED";
    private static final String MMS_DATA_TYPE = "application/vnd.wap.mms-message";

    @Override
    public void onReceive(Context context, Intent intent) {


        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Log.i(LOG_TAG, "received sms");

            //write incoming SMS to database.
            SmsMms smsMms = new SmsMms(context);
            smsMms.smsToDatabase(intent);

            //Start matrix service. This is to check Database for new messages and send them.
            Intent MatrixIntent = new Intent(context, com.gitlab.untidylamp.MatrixTexting.MatrixService.class);
            MatrixIntent.putExtra("startButton", true);
            //Startmain program (to send messages)
            context.startService(MatrixIntent);


        } else if (intent.getAction().equals(ACTION_MMS_RECEIVED) && intent.getType().equals(MMS_DATA_TYPE)) {
            Log.i(LOG_TAG, "received mms");

            try {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    //Wait 15 seconds to let default message app download mms.
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            Log.i(LOG_TAG, "Checking for new MMS.");
                            Intent intnet = new Intent(context, com.gitlab.untidylamp.MatrixTexting.MmsService.class);
                            context.startService(intnet);
                        }
                    }, 15000);
                }

            } catch (Exception e ) {
                Log.e(LOG_TAG, "received mms failed " + e);
            }
        } else {
            Log.e(LOG_TAG, "received unsupported action:  "+ intent.getAction() );
        }
    }


}
