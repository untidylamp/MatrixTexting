package com.gitlab.untidylamp.MatrixTexting;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.matrix.androidsdk.HomeServerConnectionConfig;
import org.matrix.androidsdk.MXDataHandler;
import org.matrix.androidsdk.MXSession;
import org.matrix.androidsdk.data.Room;
import org.matrix.androidsdk.data.store.IMXStore;
import org.matrix.androidsdk.data.store.IMXStoreListener;
import org.matrix.androidsdk.data.store.MXFileStore;
import org.matrix.androidsdk.listeners.IMXEventListener;
import org.matrix.androidsdk.listeners.MXMediaUploadListener;
import org.matrix.androidsdk.rest.callback.SimpleApiCallback;
import org.matrix.androidsdk.rest.client.LoginRestClient;
import org.matrix.androidsdk.rest.client.RoomsRestClient;
import org.matrix.androidsdk.rest.model.CreatedEvent;
import org.matrix.androidsdk.rest.model.Event;
import org.matrix.androidsdk.rest.model.MatrixError;
import org.matrix.androidsdk.rest.model.login.Credentials;

import org.matrix.androidsdk.rest.model.message.Message;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class Matrix {
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.Matrix";
    public final static String NOTIFICATION_CHANNEL_ID = "MatrixTexting";
    private Boolean loginFailed = false;
    private RoomsRestClient roomApi;
    private HomeServerConnectionConfig homeServer;
    private LoginRestClient client;
    private MXSession session;
    private MXDataHandler dataHandeler;
    private IMXEventListener matrixEventListener;
    private IMXStoreListener matrixStoreListener;
    private String homeServerUrl;
    private String matrixUser;
    private String botUser;
    private String botPassword;
    private String matrixSyncDelay;
    private String matrixSyncTimeout;
    private ContentResolver contentResolver;
    Context context;
    IMXStore store;

    public Matrix(final Context context, ContentResolver contentResolver, String homeServerUrl, String matrixUser, String botUser, String botPassword, String matrixSyncDelay, String matrixSyncTimeout) {
        this.context = context;
        this.homeServerUrl = homeServerUrl;
        this.matrixUser = matrixUser;
        this.botUser = botUser;
        this.botPassword = botPassword;
        this.matrixSyncDelay = matrixSyncDelay;
        this.matrixSyncTimeout = matrixSyncTimeout;
        this.contentResolver = contentResolver;
    }

    public void login () {
        homeServer = new HomeServerConnectionConfig.Builder()
                .withHomeServerUri(Uri.parse(homeServerUrl))
                .build();
        client = new LoginRestClient(homeServer);
        LoginStorage loginStorage = new LoginStorage(context);

        client.loginWithUser(botUser, botPassword, new SimpleApiCallback<Credentials>() {
            @Override
            public void onSuccess(Credentials credentials) {
                Log.i(LOG_TAG, "Login onSuccess ");
                loginFailed = false;
                homeServer.setCredentials(credentials);
                loginStorage.clear();
                loginStorage.addCredentials(homeServer);
                showNotification("Matrix Login", "Successful");
            }

            @Override
            public void onUnexpectedError(Exception e) {
                Log.e(LOG_TAG, "Login onUnexpectedError:  " + e);
                loginFailed = true;
                showNotification("Matrix Unexpected Error", String.valueOf(e));
                super.onUnexpectedError(e);
            }

            @Override
            public void onMatrixError(MatrixError e) {
                //Login Error would be here
                Log.e(LOG_TAG, "Login onMatrixError:  " + e);
                loginFailed = true;
                showNotification("Matrix Error", "Username or password incorrect.");
                super.onMatrixError(e);
            }

            @Override
            public void onNetworkError(Exception e) {
                Log.e(LOG_TAG, "Login onNetworkError:  " + e);
                loginFailed = true;
                showNotification("Matrix Network Error", String.valueOf(e));
                super.onNetworkError(e);
            }

        });
    }

    public void start() {
        if (hasCredentials()) {
            Log.i(LOG_TAG, "start Service ");
            openSessions();
            addListener();
            sendUnreadSmses();
            sendUnreadMmses();
        } else {
            showNotification("Matrix start", "Not Logged in");
        }
    }

    private void openSessions(){
        Log.i(LOG_TAG, "start startService ");
        LoginStorage test = new LoginStorage(context);
        for (HomeServerConnectionConfig config : test.getCredentialsList()) {
            homeServer = config;
        }
        client = new LoginRestClient(homeServer);
        //client.setCredentials(homeServer.getCredentials());

        store = new MXFileStore(homeServer, true, context);
        dataHandeler = new MXDataHandler(store, client.getCredentials());

        matrixStoreListener = new IMXStoreListener() {
            @Override
            public void postProcess(String accountId) {
                Log.e(LOG_TAG, "postProcess");
            }

            @Override
            public void onStoreReady(String accountId) {
                Log.e(LOG_TAG, "postProcess");
            }

            @Override
            public void onStoreCorrupted(String accountId, String description) {
                Log.e(LOG_TAG, "onStoreCorrupted");
            }

            @Override
            public void onStoreOOM(String accountId, String description) {
                Log.e(LOG_TAG, "onStoreOOM");
            }

            @Override
            public void onReadReceiptsLoaded(String roomId) {
                Log.e(LOG_TAG, "onReadReceiptsLoaded");
            }
        };


        session = new MXSession.Builder(homeServer, dataHandeler, context)
                .build();

        Log.i(LOG_TAG, "loginSuccess: session " + session );
        Log.i(LOG_TAG, "loginSuccess: setSyncDelay " + Integer.parseInt(matrixSyncDelay) );
        Log.i(LOG_TAG, "loginSuccess: setSyncTimeout " + Integer.parseInt(matrixSyncTimeout) );


        session.setSyncDelay(Integer.parseInt(matrixSyncDelay) * 1000);
        session.setSyncTimeout(Integer.parseInt(matrixSyncTimeout) * 1000);


    }

    private void addListener(){
        Log.i(LOG_TAG, "start startService ");
        roomApi = session.getRoomsApiClient();
        Log.i(LOG_TAG, "loginSuccess: roomApi " + roomApi );

        matrixEventListener = new MatrixEventListener(this, context, matrixUser);

        session.startEventStream(store.getEventStreamToken());
        session.getDataHandler().addListener(matrixEventListener);

    }

    public void sendUnreadSmses() {
        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(context);
        SQLiteDatabase smsQueryDb = mSqlOpenHelper.getReadableDatabase();
        String selectionPart = "lock=0 AND bridged=0 AND type=1";
        Cursor smsQueryCursor = smsQueryDb.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);

        while (smsQueryCursor.moveToNext()) {

            String number = smsQueryCursor.getString(smsQueryCursor.getColumnIndex("number"));
            String matrixRoom = dbManager.getMatrixRoom(mSqlOpenHelper, number);

            if (matrixRoom.startsWith("!")) {
                int id = smsQueryCursor.getInt(smsQueryCursor.getColumnIndex("_id"));
                int sourceEpoch = smsQueryCursor.getInt(smsQueryCursor.getColumnIndex("source_epoch"));
                int receivedEpoch = smsQueryCursor.getInt(smsQueryCursor.getColumnIndex("received_epoch"));
                String message = smsQueryCursor.getString(smsQueryCursor.getColumnIndex("message"));
                dbManager.smsLock(mSqlOpenHelper, id);


                Log.i(LOG_TAG, "mSqlOpenHelper: "+ mSqlOpenHelper);
                Log.i(LOG_TAG, "id " + id);
                Log.i(LOG_TAG, "sourceEpoch: " + sourceEpoch);
                Log.i(LOG_TAG, "receivedEpoch: "+ receivedEpoch);
                Log.i(LOG_TAG, "number " + number);
                Log.i(LOG_TAG, "message "+ message);
                Log.i(LOG_TAG, "roomApi "+ roomApi);

                sendMatrixText(mSqlOpenHelper, id, sourceEpoch, receivedEpoch, matrixRoom, message);
            } else if (matrixRoom.equals("LOCKED")) {
                Log.i(LOG_TAG, "room locked. Likely trying to create one.");

            } else {
                Log.i(LOG_TAG, "No room.");
                //Make room
                createMatrixRoom(mSqlOpenHelper, number);
            }
        }


        smsQueryCursor.close();
        smsQueryDb.close();
        mSqlOpenHelper.close();

    }

    public String sendMatrixText (SqlOpenHelper mSqlOpenHelper, int id, long sent, long recieved, String matrixRoom, String body ){
        String transactionId = Long.toString(System.currentTimeMillis());
        transactionId = transactionId +"_"+ id;

        Log.i(LOG_TAG, "we have a room!  " + matrixRoom);
        Message message = new Message();
        message.body = body;
        message.msgtype = "m.text";
        roomApi.sendMessage(transactionId, matrixRoom, message, new SimpleApiCallback<CreatedEvent>() {
            @Override
            public void onSuccess(CreatedEvent info) {
                Log.i(LOG_TAG, "sendMessage: sendMessage:  " + info );
                if (id > 0) {
                    dbManager.smsBridged(mSqlOpenHelper, id);
                }
            }

            @Override
            public void onUnexpectedError(Exception e) {
                if (id > 0) {
                    dbManager.smsError(mSqlOpenHelper, id, dbManager.ERROR_UNEXPECTED);
                }
                super.onUnexpectedError(e);
            }

            @Override
            public void onMatrixError(MatrixError e) {
                if (id > 0) {
                    dbManager.smsError(mSqlOpenHelper, id, dbManager.ERROR_MATRIX);
                }
                super.onMatrixError(e);
            }

            @Override
            public void onNetworkError(Exception e) {
                if (id > 0) {
                    dbManager.smsError(mSqlOpenHelper, id, dbManager.ERROR_NETWORK);
                }
                super.onNetworkError(e);
            }
        });

        return transactionId;
    }

    public void sendUnreadMmses() {

        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(context);
        SQLiteDatabase smsQueryDb = mSqlOpenHelper.getReadableDatabase();
        String mmsSelectionPart = "lock=0 AND bridged=0 AND type=1";
        Cursor mmsQueryCursor = smsQueryDb.query(DatabaseContract.MmsEntry.TABLE_NAME, null, mmsSelectionPart, null, null, null, null);

        while (mmsQueryCursor.moveToNext()) {


            String number = mmsQueryCursor.getString(mmsQueryCursor.getColumnIndex("number"));

            String matrixRoom = dbManager.getMatrixRoom(mSqlOpenHelper, number);

            if (matrixRoom.startsWith("!")){
                int id = mmsQueryCursor.getInt(mmsQueryCursor.getColumnIndex("_id"));
                int sourceEpoch = mmsQueryCursor.getInt(mmsQueryCursor.getColumnIndex("source_epoch"));
                int receivedEpoch = mmsQueryCursor.getInt(mmsQueryCursor.getColumnIndex("received_epoch"));
                String mmsId = mmsQueryCursor.getString(mmsQueryCursor.getColumnIndex("mms_id"));
                dbManager.mmsLock(mSqlOpenHelper, id);

                String selectionPart = "mid=" + String.valueOf(mmsId) + " AND NOT ct=\'application/smil\'";
                Uri uri = Uri.parse("content://mms/part");
                Cursor mmsPartCursor = contentResolver.query(uri, null, selectionPart, null, null );

                //The part table will have 2+ entries with the same mid.
                //The first is information about the rest - application/smil - I ignore this
                //The second++ is each picture set to us
                //The Last (if provided) is the text message along with all the pictures - text/plain
                //So an MMS with 2 pictures with text will have 4 parts. information, picture, picture, text.
                while (mmsPartCursor.moveToNext()) {
                    String type = mmsPartCursor.getString(mmsPartCursor.getColumnIndex("ct"));

                    if (type.equals("text/plain")){
                        String message = mmsPartCursor.getString(mmsPartCursor.getColumnIndex("text"));
                        Log.i(LOG_TAG, "MMS message with text:  " + message );
                        sendMatrixText(mSqlOpenHelper, 0, sourceEpoch, receivedEpoch, matrixRoom, message);
                    } else {
                        String name = mmsPartCursor.getString(mmsPartCursor.getColumnIndex("name"));
                        Log.i(LOG_TAG, "MMS message picture:  " + name );
                        byte[] mediaData = null;
                        String fileName = "";

                        fileName = mmsPartCursor.getString(mmsPartCursor.getColumnIndex("name"));
                        Log.e(LOG_TAG, "mmsPartCursor - fileName " + fileName);

                        mediaData = readMMSPart(mmsPartCursor.getString(mmsPartCursor.getColumnIndex("_id")));
                        Log.e(LOG_TAG, "mmsPartCursor - mediaData " + mediaData);

                        String contentType = mmsPartCursor.getString(mmsPartCursor.getColumnIndex("ct"));
                        Log.e(LOG_TAG, "mmsPartCursor - contentType " + contentType);

                        if (isImageType(contentType)) {
                            sendMatrixFile(mSqlOpenHelper, id, matrixRoom, mediaData, fileName, "m.image", contentType);
                        } else if (isVideoType(contentType)) {
                            sendMatrixFile(mSqlOpenHelper, id, matrixRoom, mediaData, fileName, "m.video", contentType);
                        }
                    }
                }


                Log.i(LOG_TAG, "mSqlOpenHelper: "+ mSqlOpenHelper);
                Log.i(LOG_TAG, "id " + id);
                Log.i(LOG_TAG, "sourceEpoch: " + sourceEpoch);
                Log.i(LOG_TAG, "receivedEpoch: "+ receivedEpoch);
                Log.i(LOG_TAG, "number " + number);
                Log.i(LOG_TAG, "message "+ mmsId);
            } else if (matrixRoom.equals("LOCKED")) {
                Log.i(LOG_TAG, "room locked. try again later");
            } else {
                Log.i(LOG_TAG, "No room.");
                //Make room
                createMatrixRoom(mSqlOpenHelper, number);
            }
        }


        mmsQueryCursor.close();
        smsQueryDb.close();
        mSqlOpenHelper.close();

    }

    public String sendMatrixFile (SqlOpenHelper mSqlOpenHelper, int id, String matrixRoom, byte[] mediaData, String fileName, String type, String contentType ){

        Log.e(LOG_TAG, "sendMatrixFile ");

        final String transactionId = Long.toString(System.currentTimeMillis())+"_"+ id;
        session.getMediaCache().uploadContent( new ByteArrayInputStream(mediaData), fileName, contentType, transactionId, new MXMediaUploadListener() {
                    @Override
                    public void onUploadComplete(final String uploadId, final String contentUri) {
                        Log.e(LOG_TAG, "sendMatrixFile onUploadComplete ");

                        JsonObject json = new JsonObject();
                        json.addProperty("body", fileName);
                        json.addProperty("msgtype", type);
                        json.addProperty("url", contentUri);
                        JsonObject info = new JsonObject();
                        info.addProperty("mimetype", contentType);
                        json.add("info", info);

                        roomApi.sendEventToRoom(
                                transactionId,
                                matrixRoom,
                                "m.room.message",
                                json,
                                new SimpleApiCallback<CreatedEvent>() {
                                    @Override
                                    public void onSuccess(CreatedEvent createdEvent) {
                                        Log.e(LOG_TAG, "sendMatrixFile onUploadComplete onSuccess");
                                        dbManager.mmsBridged(mSqlOpenHelper, id);
                                    }

                                    @Override
                                    public void onNetworkError(Exception e) {
                                        dbManager.mmsError(mSqlOpenHelper, id, dbManager.ERROR_NETWORK);
                                        super.onNetworkError(e);
                                    }

                                    @Override
                                    public void onMatrixError(MatrixError e) {
                                        dbManager.mmsError(mSqlOpenHelper, id, dbManager.ERROR_MATRIX);
                                        super.onMatrixError(e);
                                    }

                                    @Override
                                    public void onUnexpectedError(Exception e) {
                                        dbManager.mmsError(mSqlOpenHelper, id, dbManager.ERROR_UNEXPECTED);
                                        super.onUnexpectedError(e);
                                    }
                                }
                        );

                    }

                    @Override
                    public void onUploadCancel(String uploadId) {
                        dbManager.mmsError(mSqlOpenHelper, id, dbManager.ERROR_UPLOAD_CANCELED);
                        super.onUploadCancel(uploadId);
                    }

                    @Override
                    public void onUploadError(String uploadId, int serverResponseCode, String serverErrorMessage) {
                        dbManager.mmsError(mSqlOpenHelper, id, dbManager.ERROR_UPLOAD_ERROR);
                        super.onUploadError(uploadId, serverResponseCode, serverErrorMessage);
                    }
                }
        );
        return transactionId;
    }

    public void createMatrixRoom(SqlOpenHelper mSqlOpenHelper, String number) {
        Log.i(LOG_TAG, "createMatrixRoom" );
        long databaseRoomId = dbManager.newRoom(mSqlOpenHelper, number);

        session.createDirectMessageRoom(matrixUser, new SimpleApiCallback<String>() {
            @Override
            public void onSuccess(String matrixRoomId) {
                Log.i(LOG_TAG, "matrixRoomId: " + matrixRoomId );
                dbManager.updateNewRoom(mSqlOpenHelper,databaseRoomId,matrixRoomId);
                String contact = getContact(number);
                setRoomUsername(matrixRoomId, contact);
                setRoomTopic(matrixRoomId, number);
                setRoomName(matrixRoomId, contact);
            }

            @Override
            public void onNetworkError(Exception e) {
                Log.e(LOG_TAG, "onNetworkErroronNetworkError " );
                dbManager.deleteRoom(mSqlOpenHelper, Long.toString(databaseRoomId));
                super.onNetworkError(e);
            }

            @Override
            public void onMatrixError(MatrixError e) {
                Log.e(LOG_TAG, "onMatrixErroronMatrixError " );
                dbManager.deleteRoom(mSqlOpenHelper, Long.toString(databaseRoomId));
                super.onMatrixError(e);
            }

            @Override
            public void onUnexpectedError(Exception e) {
                Log.e(LOG_TAG, "onUnexpectedErroronUnexpectedError " );
                dbManager.deleteRoom(mSqlOpenHelper, Long.toString(databaseRoomId));
                super.onUnexpectedError(e);
            }
        });

    }

    public void setRoomTopic(String roomId, String topic) {
        roomApi.updateTopic(roomId, topic, new SimpleApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) {

            }
        });
    }

    public void setRoomName(String roomId, String roomName) {
        roomApi.updateRoomName(roomId, roomName, new SimpleApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) {

            }
        });
    }

    private void setRoomUsername(String roomId, String username) {
        Map<String, Object> contentPack = new HashMap<>();
        contentPack.put("displayname", username);
        contentPack.put("membership", "join");
        roomApi.sendStateEvent(roomId, "m.room.member", session.getMyUserId(), contentPack, new SimpleApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) {

            }
        });
    }

    private void setRoomRead(Room room){
        room.markAllAsRead(new SimpleApiCallback<Void>() {
            @Override
            public void onSuccess(Void info) { }
        });
    }

    private String getContact(final String number) {
        Uri uri=Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI,Uri.encode(number));
        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME};
        String contact="";

        Cursor cursor=context.getContentResolver().query(uri,projection,null,null,null);
        if (cursor != null) {
            if(cursor.moveToFirst()) {
                contact=cursor.getString(0);
            }
        }
        cursor.close();

        if (contact.isEmpty()) {
            contact = number;
        }

        return contact;
    }

    public boolean isOnline(){
        if (roomApi !=  null) {
            return true;
        } else {
            return false;
        }
    }

    public void matrixRoomMessage(Room room, Event event){
        Log.i(LOG_TAG, "matrixRoomMessage ");

        String roomId = room.getRoomId();
        long recievedEpoch= System.currentTimeMillis();
        long sourceEpoch = event.originServerTs;
        JsonObject json = event.getContent().getAsJsonObject();
        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(context);

        String number = dbManager.getMatrixRoomNumber(mSqlOpenHelper, roomId);


        Log.i(LOG_TAG, "Phone number: " + number);



        if (json.get("msgtype").getAsString().equals("m.text") && !number.isEmpty()) {
            String message = json.get("body").getAsString();
            Log.i(LOG_TAG, "Body of text: " + message);
            dbManager.importNewSms(mSqlOpenHelper, dbManager.TYPE_OUTGOING, sourceEpoch, recievedEpoch, number, message, dbManager.BRIDGED_NO, dbManager.LOCK_NO, dbManager.ERROR_NO);
        } else if (json.get("msgtype").getAsString().equals("m.image") && !number.isEmpty()) {
            //todo: find way to send this as MMS.
            String mmsUrl = session.getContentManager().getDownloadableUrl(json.get("url").getAsString());
            dbManager.importNewMms(mSqlOpenHelper, dbManager.TYPE_OUTGOING, sourceEpoch, recievedEpoch, number, sourceEpoch, mmsUrl, dbManager.BRIDGED_NO, dbManager.LOCK_NO, dbManager.ERROR_NO);
        } else if ( !number.isEmpty() ) {
            //Send a link for all the other things
            String url = session.getContentManager().getDownloadableUrl(json.get("url").getAsString());
            dbManager.importNewMms(mSqlOpenHelper, dbManager.TYPE_OUTGOING, sourceEpoch, recievedEpoch, number, sourceEpoch, url, dbManager.BRIDGED_NO, dbManager.LOCK_NO, dbManager.ERROR_NO);
        } else {
            sendMatrixError(roomId, "Can not find phone number in App database. Please update the topic with the destination cellphone number.");
        }

        setRoomRead(room);
        mSqlOpenHelper.close();

    }

    public void matrixRoomMember(Room room, Event event){
        Log.i(LOG_TAG, "matrixRoomMember ");

        JsonObject json = event.getContent().getAsJsonObject();
        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(context);


        if (json.get("membership").getAsString().equals("leave")) {
            room.leave(new SimpleApiCallback<Void>() {
                @Override
                public void onSuccess(Void info) {
                    dbManager.deleteRoom(mSqlOpenHelper, room.getRoomId());
                }
            });
        } else if (json.get("membership").getAsString().equals("invite")) {
            dbManager.addMatrixRoom(mSqlOpenHelper, room.getRoomId());
            room.join(new SimpleApiCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                }
            });
        }
        mSqlOpenHelper.close();
    }

    public void matrixRoomTopic(Room room, Event event){
        Log.i(LOG_TAG, "matrixRoomTopic ");

        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(context);

        //If we can update topic in the database
        if (dbManager.updateRoomTopic(mSqlOpenHelper,room.getRoomId(), room.getTopic())) {
            String number = room.getTopic();
            String matrixRoomId = room.getRoomId();
            String contact = getContact(number);
            setRoomUsername(matrixRoomId, contact);
            setRoomTopic(matrixRoomId, number);
            setRoomName(matrixRoomId, contact);

        } else {
            String matrixRoomId = room.getRoomId();

            sendMatrixError(matrixRoomId, "Topic already set. If there's a problem, leave this room and make another.");
            String number = dbManager.getMatrixRoomNumber(mSqlOpenHelper, matrixRoomId);
            setRoomTopic(matrixRoomId, number);

        }
        setRoomRead(room);
        mSqlOpenHelper.close();

    }

    public String sendMatrixError ( String matrixRoom, String body ){
        String transactionId = Long.toString(System.currentTimeMillis());
        transactionId = transactionId;

        Message message = new Message();
        message.body = "!!!ERROR!!! " + body;
        message.msgtype = "m.emote";
        roomApi.sendMessage(transactionId, matrixRoom, message, new SimpleApiCallback<CreatedEvent>() {
            @Override
            public void onSuccess(CreatedEvent info) {
                Log.i(LOG_TAG, "sendMatrixError: sendMatrixError:  " + info );
            }

        });

        return transactionId;
    }

    public void showNotification(String title, String body) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("title")
                .setContentText("content")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mBuilder = new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setContentText(body)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        MainActivity.notificationManager.notify(001, mBuilder.build());
    }

    private byte[] readMMSPart(String partId) {
        byte[] partData = null;
        Uri partURI = Uri.parse("content://mms/part/" + partId);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = null;

        try {

            Log.i(LOG_TAG,"Entered into readMMSPart try.");

            is = contentResolver.openInputStream(partURI);

            byte[] buffer = new byte[256];
            int len = is.read(buffer);
            while (len >= 0) {
                baos.write(buffer, 0, len);
                len = is.read(buffer);
            }
            partData = baos.toByteArray();

        } catch (IOException e) {
            Log.e(LOG_TAG, "Exception == Failed to load part data");
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Exception :: Failed to close stream");
                }
            }
        }
        return partData;
    }

    private boolean isImageType(String mime) {
        boolean result = false;
        if (mime.equalsIgnoreCase("image/jpg")
                || mime.equalsIgnoreCase("image/jpeg")
                || mime.equalsIgnoreCase("image/png")
                || mime.equalsIgnoreCase("image/gif")
                || mime.equalsIgnoreCase("image/bmp")) {
            result = true;
        }
        return result;
    }

    private boolean isVideoType(String mime) {
        boolean result = false;
        if (mime.equalsIgnoreCase("video/3gpp")
                || mime.equalsIgnoreCase("video/3gpp2")
                || mime.equalsIgnoreCase("video/avi")
                || mime.equalsIgnoreCase("video/mp4")
                || mime.equalsIgnoreCase("video/mpeg")
                || mime.equalsIgnoreCase("video/webm")) {
            result = true;
        }
        return result;
    }

    public boolean hasCredentials(){
        LoginStorage loginStorage = new LoginStorage(context);
        if (loginStorage.getCredentialsList().isEmpty()) {
            return false;
        } else {
            return true;
        }

    }

    public void logout(){
        LoginStorage loginStorage = new LoginStorage(context);
        loginStorage.clear();
    }

    public void stop() {
        Log.i(LOG_TAG, "matrix stop called");
        if (isOnline()) {
            store.removeMXStoreListener(matrixStoreListener);
            dataHandeler.removeListener(matrixEventListener);
            session.clear(context);
            session.stopEventStream();
            store.close();
        }
    }
}
