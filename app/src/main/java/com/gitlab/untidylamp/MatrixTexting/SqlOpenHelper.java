package com.gitlab.untidylamp.MatrixTexting;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqlOpenHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.SqlOpenHelper";
    public  static final String DATABASE_NAME = "MatrixTexting.db";
    public static final int DATABASE_VERSION = 1;


    public SqlOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseContract.SmsEntry.SQL_CREATE_TABLE);
        db.execSQL(DatabaseContract.MmsEntry.SQL_CREATE_TABLE);
        db.execSQL(DatabaseContract.RoomsEntry.SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
