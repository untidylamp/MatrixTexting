package com.gitlab.untidylamp.MatrixTexting;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SmsMms {
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.SmsMms";
    Context context;

    public SmsMms(Context context) {
        this.context = context;
    }

    public Boolean smsToDatabase(Intent intent) {

        Map<String, String> msg = null;
        SmsMessage[] msgs = null;
        Bundle bundle = intent.getExtras();
        long newSmsId = 0;

        if (bundle != null && bundle.containsKey("pdus")) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(context);

            if (pdus != null) {
                int nbrOfpdus = pdus.length;
                msg = new HashMap<String, String>(nbrOfpdus);
                msgs = new SmsMessage[nbrOfpdus];

                // Send long SMS of same sender in one message
                for (int i = 0; i < nbrOfpdus; i++) {
                    msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);

                    String number = msgs[i].getOriginatingAddress();

                    // Check if index with number exists
                    if (!msg.containsKey(number)) {
                        // Index with number doesn't exist
                        long sourceEpoch = msgs[i].getTimestampMillis();
                        long recievedEpoch= System.currentTimeMillis();
                        String message = msgs[i].getMessageBody();
                        Log.i(LOG_TAG, "Import new SMS: " + number );
                        newSmsId = dbManager.importNewSms(mSqlOpenHelper, dbManager.TYPE_INCOMING, sourceEpoch, recievedEpoch, number, message, dbManager.BRIDGED_NO, dbManager.LOCK_NO, dbManager.ERROR_NO );
                        msg.put(number, msgs[i].getMessageBody());

                    } else {
                        if (newSmsId > 0) {
                            // Number is there.
                            String previousparts = msg.get(number);
                            String msgString = previousparts + msgs[i].getMessageBody();
                            msg.put(number, msgString);
                            Log.i(LOG_TAG, "Update Long SMS: " + number );
                            dbManager.updateLongSmsBody(mSqlOpenHelper, newSmsId, msgString);
                        }
                    }
                }
            }
            mSqlOpenHelper.close();
            return true;
        } else {
            return false;
        }
    }

    public void sendUnreadSmses() {
        Log.i(LOG_TAG, "sendUnreadSmses" );
        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(context);
        SQLiteDatabase db = mSqlOpenHelper.getReadableDatabase();
        String selectionPart = "lock=0 AND bridged=0 AND type=2";
        Cursor smsQueryCursor = db.query(DatabaseContract.SmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);

        while (smsQueryCursor.moveToNext()) {
            int id = smsQueryCursor.getInt(smsQueryCursor.getColumnIndex("_id"));
            String number = smsQueryCursor.getString(smsQueryCursor.getColumnIndex("number"));
            String message = smsQueryCursor.getString(smsQueryCursor.getColumnIndex("message"));
            dbManager.smsLock(mSqlOpenHelper, id);

            Log.i(LOG_TAG, "mSqlOpenHelper: "+ mSqlOpenHelper);
            Log.i(LOG_TAG, "id " + id);
            Log.i(LOG_TAG, "number " + number);
            Log.i(LOG_TAG, "message "+ message);

            sendSmsMessage(mSqlOpenHelper, number, message, id);
        }

        mSqlOpenHelper.close();
        smsQueryCursor.close();
        db.close();

    }

    public void sendUnreadMmsesAsUrl() {
        Log.i(LOG_TAG, "sendUnreadMmsesAsUrl" );
        SqlOpenHelper mSqlOpenHelper = new SqlOpenHelper(context);
        SQLiteDatabase db = mSqlOpenHelper.getReadableDatabase();
        String selectionPart = "lock=0 AND bridged=0 AND type=2";
        Cursor mmsQueryCursor = db.query(DatabaseContract.MmsEntry.TABLE_NAME, null, selectionPart, null, null, null, null);

        while (mmsQueryCursor.moveToNext()) {
            int id = mmsQueryCursor.getInt(mmsQueryCursor.getColumnIndex("_id"));
            String number = mmsQueryCursor.getString(mmsQueryCursor.getColumnIndex("number"));
            String url = mmsQueryCursor.getString(mmsQueryCursor.getColumnIndex("url"));
            dbManager.smsLock(mSqlOpenHelper, id);

            Log.i(LOG_TAG, "mSqlOpenHelper: "+ mSqlOpenHelper);
            Log.i(LOG_TAG, "id " + id);
            Log.i(LOG_TAG, "number " + number);
            Log.i(LOG_TAG, "url "+ url);

            sendUrlMessage(mSqlOpenHelper, number, url, id);
        }

        mSqlOpenHelper.close();
        mmsQueryCursor.close();
        db.close();

    }

    public void sendSmsMessage(SqlOpenHelper mSqlOpenHelper, String number, String message, long db_id) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> body = smsManager.divideMessage(message);
        smsManager.sendMultipartTextMessage(number, null, body, null, null);
        //TODO: Check if the messages was actually sent before marking as bridged.
        dbManager.smsBridged(mSqlOpenHelper, db_id);
    }

    public void sendUrlMessage(SqlOpenHelper mSqlOpenHelper, String number, String message, long db_id) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> body = smsManager.divideMessage(message);
        smsManager.sendMultipartTextMessage(number, null, body, null, null);
        //TODO: Check if the messages was actually sent before marking as bridged.
        dbManager.mmsBridged(mSqlOpenHelper, db_id);
    }

}
