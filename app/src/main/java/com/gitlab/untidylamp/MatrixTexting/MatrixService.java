package com.gitlab.untidylamp.MatrixTexting;

import android.app.Service;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.matrix.androidsdk.HomeServerConnectionConfig;

public class MatrixService extends Service {
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.MatrixService";
    HomeServerConnectionConfig hsConfig;
    private Matrix matrix = null;
    private SharedPreferences settings;
    private String homeServerUrl;
    private String matrixUser;
    private String botUser;
    private String botPassword;
    private String matrixSyncDelay;
    private String matrixSyncTimeout;
    private ContentResolver contentResolver;
    public final static String NOTIFICATION_CHANNEL_ID = "MatrixTexting";


    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG_TAG, "onStartCommand");

        settings = getSharedPreferences("settings", Context.MODE_PRIVATE);
        homeServerUrl = settings.getString("homeServerUrl", "");
        matrixUser = settings.getString("matrixUser", "");
        botUser = settings.getString("botUser", "");
        matrixSyncDelay = settings.getString("matrixSyncDelay", "");
        matrixSyncTimeout = settings.getString("matrixSyncTimeout", "");
        contentResolver = getContentResolver();


        if (!homeServerUrl.isEmpty() &&
                !matrixUser.isEmpty() &&
                !botUser.isEmpty() &&
                !matrixSyncDelay.isEmpty() &&
                !matrixSyncTimeout.isEmpty() ) {

            if (matrix == null) {
                matrix = new Matrix(getApplicationContext(), contentResolver, homeServerUrl, matrixUser, botUser, null, matrixSyncDelay, matrixSyncTimeout);
                matrix.start();
            } else if (matrix != null && matrix.isOnline()) {
                matrix.sendUnreadSmses();
                matrix.sendUnreadMmses();
            } else {
                Log.i(LOG_TAG, "We are in the middle of a login.");
                //For Example, the service is not running and we get two SMSes within seconds.
                //The first one will start the login and the seconds will see matrix is not null,
                //try to send messages, and crash because we are not logged in yet.
            }

        } else {
            Log.i(LOG_TAG, "Missing Information");
            showNotification("Matrix Settings", "Missing Information");
            stopSelf();
        }


        Log.i(LOG_TAG, "onStartCommand END");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(LOG_TAG, "onDestroy: MatrixService");
        if (matrix != null) {
            matrix.stop();
        }
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void showNotification(String title, String body) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("title")
                .setContentText("content")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        mBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setContentText(body)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        MainActivity.notificationManager.notify(001, mBuilder.build());
    }
}
