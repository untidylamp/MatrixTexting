package com.gitlab.untidylamp.MatrixTexting;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_CONTACTS;
import static android.Manifest.permission.READ_SMS;
import static android.Manifest.permission.RECEIVE_MMS;
import static android.Manifest.permission.RECEIVE_SMS;
import static android.Manifest.permission.SEND_SMS;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    private static final String LOG_TAG = "com.gitlab.untidylamp.MatrixTexting.MainActivity";
    private static final String[] PERMISSIONS_REQUIRED = new String[]{ READ_SMS, SEND_SMS, RECEIVE_MMS, RECEIVE_SMS, INTERNET, READ_CONTACTS};
    private static final int PERMISSION_REQUEST_CODE = 200;
    public static NotificationManager notificationManager = null;
    public final static String NOTIFICATION_CHANNEL_ID = "MatrixTexting";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!checkPermission()) {
            requestPermission();
        }

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final TextView serviceStatus = findViewById(R.id.status_service);

        buildNotificationManager();



        Object startButton = findViewById(R.id.start_button);
        ((View) startButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View startServiceView) {
                Log.i(LOG_TAG, "startButton - onClick");
                Intent mmsIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MmsService.class);
                mmsIntent.putExtra("startButton", true);
                startService(mmsIntent);

                Intent MatrixIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MatrixService.class);
                MatrixIntent.putExtra("startButton", true);
                startService(MatrixIntent);

                Log.i(LOG_TAG, "startButton - onClick done");
            }
        });
        final Object stopButton = findViewById(R.id.stop_button);
        ((View) stopButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(LOG_TAG, "stopButton - onClick");
                Intent mmsIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MmsService.class);
                stopService(mmsIntent);

                Intent MatrixIntent = new Intent(getApplicationContext(), com.gitlab.untidylamp.MatrixTexting.MatrixService.class);
                stopService(MatrixIntent);

                Log.i(LOG_TAG, "stopButton - onClick done");
            }
        });

        final Object statusButton = findViewById(R.id.status_button);
        ((View) statusButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceStatus.setText(String.valueOf(isMyServiceRunning(MatrixService.class)));

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        if (id == R.id.smsmms) {
            startActivity(new Intent(this, SmsMmsActivity.class));
            return true;
        }
        if (id == R.id.rooms) {
            startActivity(new Intent(this, RoomsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkPermission() {
        //for each permission
        for (String permission: PERMISSIONS_REQUIRED) {
            int result = ContextCompat.checkSelfPermission(getApplicationContext(), permission);
            if (result  != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS_REQUIRED, PERMISSION_REQUEST_CODE);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(getApplicationContext().ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void buildNotificationManager() {

        notificationManager = getSystemService(NotificationManager.class);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Matrix Service";
            String description = "description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance);
            channel.setDescription(description);
            notificationManager.createNotificationChannel(channel);
        }
    }

}
